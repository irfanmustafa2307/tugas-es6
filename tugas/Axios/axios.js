//const axios = require('axios');

const postsList = document.querySelector('.post-list');
const addPostForm = document.querySelector('.add-post-form');
const titleValue = document.getElementById('title-value');
const contentValue = document.getElementById('content-value');
const modalTitle = document.getElementById('modal-title');
const modalContent = document.getElementById('modal-content');
//const btnSubmit = document.querySelector('.btn');
const btnUpdate = document.querySelector('.btn-update');

const url = 'https://edi-server.herokuapp.com/posts';

//METHOD GET AXIOS
axios.get(url).then(res => {
    const table = document.getElementById('table');
    table.innerHTML = createTable(res.data);

    postsList.addEventListener('click', (e) => {
        e.preventDefault();
        let delButtonisPressed = e.target.id == 'delete-post'
        let editButtonisPressed = e.target.id == 'edit-post'

        // console.log(e.target.parentElement.dataset.id)
        let id = e.target.parentElement.dataset.id;

        //METHOD DELETE
        if (delButtonisPressed) {
            axios.delete(`${url}/${id}`)
                .then(() => location.reload())
        }

        //METHOD PUT
        if (editButtonisPressed) {
            const parent = e.target.parentElement;
            console.log(parent)
            let inputTitle = parent.querySelector('.input-title').textContent;
            let inputContent = parent.querySelector('.input-content').textContent;
            //console.log(inputTitle, inputContent)

            modalTitle.value = inputTitle;
            modalContent.value = inputContent;
        }

        btnUpdate.addEventListener('click', (e) => {
            e.preventDefault()
            axios.put(`${url}/${id}`, {
                    title: modalTitle.value,
                    content: modalContent.value,
                })
                .then(() => location.reload())
        })
    })
})

const createTable = data => {
    let table = '';
    data.map(d => {
        table += `
            <tr >
            <td>${d.id}</td>
            <td class="input-title">${d.title}</td>
            <td>${d.content}</td>
            <td data-id=${d.id}>
            <p class="input-title hidden">${d.title}</p>
            <p class="input-content hidden">${d.content}</p>
            <a id="edit-post" type="button" class="btn btn-sm btn-warning text-white" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Update</a>
            <a class="btn btn-sm btn-danger" id="delete-post" value=${d.id}>Delete</a>
            </td>
            </tr>
         
        `;
    })
    return table;
}


//POST AXIOS
addPostForm.addEventListener('submit', (e) => {
    e.preventDefault();
    axios.post(url, {
            title: titleValue.value,
            content: contentValue.value
        })
        .then(data => {
            const dataArr = [];
            dataArr.push(data);
            createTable(dataArr);

        })
        .then(() => location.reload())
})