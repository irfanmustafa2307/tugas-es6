//contoh Asyncronous
// document.write("selamat datang");

// setTimeout(() => {
//     document.write("Selamat berhitung")
// }, 10000)

// document.write("di program callback");

function hitung(par1,par2,callback){
    //default operation
    result = par1 + par2
    
    // callback is function ?
    if (typeof callback == 'function'){
     result= callback(par1,par2)
     document.write(`</br>Jika x = ${par1} dan y = ${par2} </br>`);
    }
    
    return result
  }
  
  //execute asyncronous
  setTimeout(() => {
  a=hitung(10,5, function(x,y){return "Hasil Penjumlahan nya adalah " + (x + y) }) 
  document.write(a)
}, 50000)

setTimeout(() => {
  b=hitung(20,5, function(x,y){return "Hasil Perkalian nya adalah "  + (x * y) }) 
  document.write(b)
}, 7000)

setTimeout(() => {
  c=hitung(15,5, function(x,y){return "Hasil Pengurangan nya adalah "  + (x - y) }) 
  document.write(c)
}, 2000)

setTimeout(() => {
  d=hitung(15,3, function(x,y){return "Hasil Pembagian nya adalah "  + (x / y) }) 
  document.write(d)
}, 9000)

setTimeout(() => {
  e=hitung(17,4, function(x,y){return "Hasil Modulus nya adalah "  + (x % y) }) 
  document.write(e)
}, 10000)

setTimeout(() => {
    document.write("Lihat Hasilnya")
  }, 1000)
  

 


  //cara lain
// function hitung(x, y, callback) {
//     const operasi = callback(x, y)
//     console.log(`x = ${x}, y = ${y}`);
//     console.log("hasil " + operasi);
// }

// const penjumlahan = function (angka1, angka2) {
//     console.log("Fungsi Penjumlahan");
//     return angka1 + angka2;
// };
// const perkalian = function (angka1, angka2) {
//     console.log("Fungsi Perkalian");
//     return angka1 * angka2;
// };

// const pengurangan = function (angka1, angka2) {
//     console.log("Fungsi Pengurangan");
//     return angka1 - angka2;
// };

// const pembagian = function (angka1, angka2) {
//     console.log("Fungsi Pembagian");
//     return angka1 / angka2;
// };

// hitung(10, 5, penjumlahan);
// hitung(10, 5, perkalian);
// hitung(10, 5, pengurangan);
// hitung(10, 5, pembagian);




