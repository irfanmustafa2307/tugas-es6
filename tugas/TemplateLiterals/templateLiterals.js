const data = [
{    
    nama: 'Edi Hartono',
    umur: 23,
    alamat: 'Karangsono'
}, 
{    
    nama: 'M. Rizal Aldosari',
    umur: 24, 
    alamat: 'Kuripan'
},
{   nama: 'Wahyu Setiawan',   
    umur: 25,    
    alamat: 'Bumi Rejo'
}, 
{   
    nama: 'Mahesa Anggraestian',   
    umur: 20,    
    alamat: 'Tawangsari'
},
{
    nama: 'Edow Bagus Pangestu',  
    umur: 28, 
    alamat: 'Tlogosari'
}
];

const el = `
<div class="d-grid gap-2 col-6 mx-auto mt-5">
<button class="btn btn-success"  onclick="tampil()" type="button">Lihat Hasil</button>
</div>

    <div id="dataTable">

<div class="data mt-5">
<table class="table  mr-5">
 <thead class="table-dark">
   <tr>
     <th scope="col">Nama</th>
     <th scope="col">Umur</th>
     <th scope="col">Alamat</th>
   </tr>
 </thead>
 <tbody>
 ${data.map(d =>`<tr>
     <td>${d.nama}</td>
     <td>${d.umur}</td>
     <td>${d.alamat}</td>
   </tr>`).join('')}
 </tbody>
</table>
</div>
</div>
<div class="d-grid gap-2 col-6 mx-auto mt-5">
<a class="btn btn-secondary" type="button" href="../../../index.html">Kembali</a>
</div>
`;

 document.body.innerHTML = el

 function tampil(){
    const aktif = document.getElementById("dataTable");
        if (aktif.style.display === "none") {
            aktif.style.display = "block";
        } else {
            aktif.style.display = "none";
        }
}
tampil()


// const el = `<div class="data">
//  ${data.map(d => `<ul>
//     <li>${d.nama}</li>
//     <li>${d.umur}</li>
//     <li>${d.alamat}</li>
//  </ul>`).join('')}
// </div>
// `;

// document.body.innerHTML = el



// const output = data.map(({ nama, umur, alamat }) => {
//     return `
    
//     `
// });

// const str = 

// console.table(output);

// // document.body.innerHTML  = output;



