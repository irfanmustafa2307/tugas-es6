var person = {
    // properti
    name: "Irfan", 
    age: "21", 
    address: "Bandung",

    // method
    eat: function(){
        console.log("this is method eat");
    },
    sleep: function(){
        console.log("this is method sleep");
    },
    work: function(){
        console.log("this is method work");
    }
    
};

console.log(person.name);
console.log(person.age);

person.eat();
person.work();