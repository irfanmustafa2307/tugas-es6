const data = [    
    {
        nama: 'Edi Hartono',        
        umur: 23,
        alamat: 'Karangsono'
    },
    {
        nama: 'M. Rizal Aldosari',
        umur: 24,
        alamat: 'Kuripan',
    },
    {
        nama: 'Wahyu Setiawan',
        umur: 25,
        alamat: 'Bumi Rejo'
    },
    {
        nama: 'Mahesa Anggraestian',
        umur: 20,
        alamat: 'Tawangsari'
    },
    {
        nama: 'Edow Bagus Pangestu',
        umur: 28,
        alamat: 'Tlogosari'
    }
];

const btnClick = document.getElementById('click-me');
btnClick.addEventListener('click', () => {
    alert('Lihat Console CTRL + SHIFT + I !');

const reducer = (accumulator, currentValue) => accumulator + currentValue;

const totalUmur = data.map((data)=> data.umur).filter( age => age%4==0).reduce(reducer)

console.log(totalUmur);


})