const postsList = document.querySelector('.post-list');
const addPostForm = document.querySelector('.add-post-form');
const titleValue = document.getElementById('title-value')
const bodyValue = document.getElementById('body-value')
const btnSubmit = document.querySelector('.btn')
let output = '';

const url = 'https://edi-server.herokuapp.com/posts';

const renderPosts = (posts) => {
    posts.forEach(post => {
        console.log(post)
        output += `
  <div class="card mt-4 col-md-6 bg-light">
  <div class="card-body" data-id=${post.id}>
    <h5 class="input-title">${post.title}</h5>
    <p class="input-body">${post.content}</p>
    <a href="#" id="edit-post" class="btn btn-warning">Edit</a>
    <a href="#" id="delete-post" class="btn btn-danger">Delete</a>
    
  </div>
</div>
`;
    });
    postsList.innerHTML = output;
}

//method Get
fetch(url)
    .then(res => res.json())
    .then(data => renderPosts(data))

postsList.addEventListener('click', (e) => {
    e.preventDefault();
    let delButtonisPressed = e.target.id == 'delete-post'
    let editButtonisPressed = e.target.id == 'edit-post'

    let id = e.target.parentElement.dataset.id;
    // console.log(e.target.parentElement.dataset.id)

    //method DELETE
    if (delButtonisPressed) {
        fetch(`${url}/${id}`, {
                method: 'DELETE'
            })
            .then(res => res.json())
            .then(() => location.reload())
    }

    //membaca data ke textarea
    if (editButtonisPressed) {
        const parent = e.target.parentElement;
        let inputTitle = parent.querySelector('.input-title').textContent;
        let inputBody = parent.querySelector('.input-body').textContent;
        console.log(inputTitle, inputBody)

        titleValue.value = inputTitle;
        bodyValue.value = inputBody

    }

    btnSubmit.addEventListener('click', (e) => {
        e.preventDefault()
        fetch(`${url}/${id}`, {
                method: "PATCH",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    title: titleValue.value,
                    content: bodyValue.value,

                })
            })
            .then(res => res.json())
            .then(() => location.reload())
    })
})

//method POST
addPostForm.addEventListener('submit', (e) => {
    e.preventDefault();
    fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title: titleValue.value,
                content: bodyValue.value
            })
        })
        .then(res => res.json())
        .then(data => {
            const dataArr = [];
            dataArr.push(data);
            renderPosts(dataArr);

        })
        .then(() => location.reload())
})


//Menampilkan Tabel
let tabel = document.getElementById("tabel-body");

let api = fetch("https://edi-server.herokuapp.com/posts");

const tabelApi = api
    .then((res) => res.json())
    .then((res) => {
        res.map((post) => (tabel.innerHTML += `
            <tr>
            <td>${post.id}</td>
            <td >${post.title}</td>
            <td >${post.content}</td>
            </tr>`));
    })